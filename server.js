var express       = require("express");
var session       = require('express-session');
var path          = require("path");
var bodyParser    = require("body-parser");
var cookieParser  = require("cookie-parser");
var mongodb       = require("mongodb");
var passport      = require("passport")
  , LocalStrategy = require("passport-local").Strategy;
var flash         = require("connect-flash");

var ObjectID = mongodb.ObjectID;

var PROJECTS_COLLECTION = "projects";
var COMMENTS_COLLECTION = "comments";
var USERS_COLLECTION    = "users";
var ADMIN_COLLECTION    = "admin";
var BIN_COLLECTION      = "bin";

var app = express();
app.use(express.static(__dirname + "/www"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cookieParser({ secret: 'city360-secret', cookie: { maxAge: 60 * 60 * 1000 }}));
app.use(session({ secret: 'city360-secret', saveUninitialized: true, resave: false, cookkie: { secure: true } }));
app.use(passport.initialize());
app.use(passport.session());

app.set('trust proxy', 1);


// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGODB_URI, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  db = database;
  console.log("Database connection ready");

  // Initialize the app.
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  db.collection(USERS_COLLECTION)
    .findOne({ "_id": id }, function(err, user) {
      done(err, user);
    });
});

// PASSPORT STRATEGIES

passport.use('local-register', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function(req, email, password, done) {
    db.collection(USERS_COLLECTION)
      .findOne({ email: email }, function (err, user) {
        if (err) { 
          return done(err); 
        }
        if (user) {
          return done(null, false, { message: 'Email is already taken.' });
        }
        db.collection(USERS_COLLECTION)
          .findOne({ 'username': req.body.username }, function(err, user_){
            if (err) {
              return done(err);
            }
            if(user_){
              return done(null, false, { message: 'The username is already taken.' });
            }

            var newUser = new User();

            newUser.username = req.body.username;
            newUser.email = email;
            newUser.password = password;

            newUser.save(function(err){
              if (err) {
                if (err.errors && err.name === "ValidationError") {
                  for (var key in err.errors) {
                    req.flash('signupMessage', err.errors[key].message);
                  }
                  return done(null, false);
                }
                throw err;
              }
              return done(null, newUser);
            });
          });
      });
  }));

passport.use('local-login', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function(req, email, password, done) {
    db.collection(USERS_COLLECTION)
      .findOne({ email: email }, function (err, user) {
        if (err) { 
          return done(err); 
        }
        if (!user) {
          return done(null, false, { message: 'This email is not registered.' });
        }
        if (!user.validPassword(password)) {
          return done(null, false, { message: 'Invalid password.' });
        }
        if (user) {
          return done(null, false, { message: 'Email is already taken.' });
        }
        return done(null, user);
      });
  }));

// PROJECTS API ROUTES 

/*
 *  "/projects"
 *    GET: finds all projects
 *    POST: creates a new project
 */

app.get("/projects", function(req, res){
  db.collection(PROJECTS_COLLECTION)
    .find({})
    .sort({"createdAt": -1})
    .toArray(function(err, doc){
      if (err) {
        handleError(res, err.message, "Failed to retrieve projects from the database.");
      } else {
        res.status(200).json(doc);
      }
    });
});

app.post("/projects", function(req, res){
  var newProject = req.body;
  newProject.createdAt = new Date();

  if(!(newProject.title || 
       newProject.description || 
       newProject.location || 
       newProject.startingDateTime || 
       newProject.endingDateTime || 
       newProject.categories ||
       newProject.foundedBy)){
    handleError(res, "Insufficient Information Entered", "Please fill up all the required items", 400);
  }

  db.collection(PROJECTS_COLLECTION)
    .insertOne(newProject, function(err, doc){
      if (err) {
        handleError(res, err.message, "Failed to create the project.");
      } else{
        res.status(201).json(doc.ops[0]);
      }
    })
});

/*
 *  "/projects/:id"
 *    GET: find project by project id
 *    PUT: update project by id
 *    DELETE: delete project by id
 */

app.get("/projects/:id", function(req, res){
  db.collection(PROJECTS_COLLECTION)
    .findOne({ "_id": new ObjectID(req.params.id) }, function(err, doc){
      if (err) {
        handleError(res, err.message, "Failed to retrieve the particular project.");
      } else{
        res.status(200).json(doc);
      }
    });
});

app.put("/projects/:id", function(req, res){
  var updateProject = req.body;
  delete updateProject._id;

  db.collection(PROJECTS_COLLECTION)
    .updateOne({ "_id": new ObjectID(req.params.id) }, updateProject, function(err, doc){
      if (err) {
        handleError(res, err.message, "Failed to update the particular project.");
      } else{
        res.status(200).end();
      }
    });
});

app.delete("/projects/:id", function(req, res){
  var updateProject = req.body;
  delete updateProject._id;

  db.collection(PROJECTS_COLLECTION)
    .deleteOne({ "_id": new ObjectID(req.params.id) }, function(err, result){
      if (err) {
        handleError(res, err.message, "Failed to delete the particular project.");
      } else{
        res.status(204).end();
      }
    });
});

/*
 *  "/projects/latest/project"
 *    GET: retrieve the latest project (3 projects)
 */

app.get("/projects/latest/project", function(req, res){
  db.collection(PROJECTS_COLLECTION)
    .find({})
    .sort({"createdAt": -1})
    .limit(3)
    .toArray(function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to get the latest 3 projects");
      } else {
        res.status(200).json(doc);
      }
    });
});

/*
 *  "/projects/popular/project"
 *    GET: retrieve the most popular project (3 projects)
 */

app.get("/projects/popular/project", function(req, res){
  db.collection(PROJECTS_COLLECTION)
    .find({})
    .sort({"createdAt": -1, "likesCount": -1})
    .toArray(function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to get popular projects");
      } else {
        res.status(200).json(doc);
      }
    });
});

/*
 *  "/search/:text/"
 *    GET: retrieve the projects that have particular text
 */

// app.get("/search/:text", function(req, res){
//   db.collection(PROJECTS_COLLECTION)
//     .find({})
//     .sort({"createdAt": -1, "likesCount": -1})
//     .toArray(function(err, doc) {
//       if (err) {
//         handleError(res, err.message, "Failed to get the latest 3 projects");
//       } else {
//         res.status(200).json(doc);
//       }
//     });
// });

/*
 *  "/categories/:category/projects"
 *    GET: retrieve the projects that has the given category
 */

app.get("/categories/:category/projects", function(req, res){
  db.collection(PROJECTS_COLLECTION)
    .find({"categories": req.params.category})
    .sort({"createdAt": -1})
    .toArray(function(err, docs){
      if (err) {
        handleError(res, err.message, "Failed to retrieve projects that has the particular category from the database.");
      } else {
        res.status(200).json(docs);
      }
    });
});


// COMMENTS API ROUTES

/*
 *  "/comments"
 *    POST: post new comment
 *  "/comments/:projectId"
 *    GET: get all comments based on projectId
 */

app.post("/comments", function(req, res) {
  var newComment = req.body;
  newComment.createdAt = new Date();

  if (!(req.body.userId || req.body.projectId || req.body.comment)) {
    handleError(res, "Invalid input", "Please provide sufficient data.", 400);
  }

  db.collection(COMMENTS_COLLECTION)
    .insertOne(newComment, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to create new contact.");
      } else {
        res.status(201).json(doc.ops[0]);
      }
    });
});

app.get("/comments/:projectId", function(req, res) {
  db.collection(COMMENTS_COLLECTION)
    .find({ "projectId": req.params.projectId })
    .sort({ "_id": -1 }).toArray(function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to retrieve comments of the project");
      } else {
        res.status(200).json(doc);
      }
    });
});



// USERS API ROUTES

/*  "/users"
 *    GET: retrieve all users
 *    POST: create a new user
 */

app.get("/users", function(req, res) {
  db.collection(USERS_COLLECTION)
    .find({})
    .toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to retrieve all users.");
      } else {
        res.status(200).json(docs);
      }
    });
});

app.post("/users", function(req, res) {
  var newUser = req.body;
  newUser.createdAt = new Date();

  if (!(req.body.name || req.body.email || req.body.password)) {
    handleError(res, "Insufficient data", "Must provide a name.", 400);
  }

  db.collection(USERS_COLLECTION)
    .findOne({ email: new ObjectID(req.params.email) }, function(err, doc) {
      if (err) {
        handleError(res, err.message, "The email is already registered in the database!");
      } else {
        db.collection(USERS_COLLECTION)
          .insertOne(newUser, function(err, doc) {
            if (err) {
              handleError(res, err.message, "Failed to create the new user.");
            } else {
              res.status(201).json(doc.ops[0]);
            }
          });
      }
    });
});

/*  "/user/auth/:email"
 *    GET: find user by email
 *    PUT: update user by email
 *    DELETE: delete user by email
 */

app.get("/user/auth/:email", function(req, res) {
  db.collection(USERS_COLLECTION)
    .findOne({ email: req.params.email }, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to get user");
      } else {
        res.status(200).json(doc);
      }
    });
});

app.put("/user/auth/:email", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc.email;

  db.collection(USERS_COLLECTION)
    .updateOne({ email: req.params.email }, updateDoc, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to update user");
      } else {
        res.status(204).end();
      }
    });
});

app.delete("/user/auth/:email", function(req, res) {
  db.collection(USERS_COLLECTION)
    .deleteOne({ email: req.params.email}, function(err, result) {
      if (err) {
        handleError(res, err.message, "Failed to delete user");
      } else {
        res.status(204).end();
      }
    });
});

/*  "/admin/auth/:email"
 *    GET: find user by email
 *    PUT: update user by email
 *    DELETE: delete user by email
 */

app.get("/admin/auth/:email", function(req, res) {
  db.collection(ADMIN_COLLECTION)
    .findOne({ email: req.params.email }, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to get user");
      } else {
        res.status(200).json(doc);
      }
    });
});

app.put("/admin/auth/:email", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc.email;

  db.collection(ADMIN_COLLECTION)
    .updateOne({ email: req.params.email }, updateDoc, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to update user");
      } else {
        res.status(204).end();
      }
    });
});

app.delete("/admin/auth/:email", function(req, res) {
  db.collection(ADMIN_COLLECTION)
    .deleteOne({ email: req.params.email}, function(err, result) {
      if (err) {
        handleError(res, err.message, "Failed to delete user");
      } else {
        res.status(204).end();
      }
    });
});

/*  "/user/:id"
 *    GET: find user by id
 *    PUT: update user by id
 *    DELETE: delete user by id
 */

app.get("/user/:id", function(req, res) {
  db.collection(USERS_COLLECTION)
    .findOne({ _id: req.params.id }, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to get user");
      } else {
        res.status(200).json(doc);
      }
    });
});

app.put("/user/:id", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc.email;

  db.collection(USERS_COLLECTION)
    .updateOne({ _id: req.params.id }, updateDoc, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to update user");
      } else {
        res.status(204).end();
      }
    });
});

app.delete("/user/:id", function(req, res) {
  db.collection(USERS_COLLECTION)
    .deleteOne({ _id: req.params.id}, function(err, result) {
      if (err) {
        handleError(res, err.message, "Failed to delete user");
      } else {
        res.status(204).end();
      }
    });
});

// USER PROJECTS API ROUTES

/*
 *  "/user/:id/projects"
 *    GET: get projects of particular user
 *    POST: post projects to particular user
 *    DELETE: delete projects of particular user
 */

app.get("/user/:id/projects", function(req, res){
  db.collection(PROJECTS_COLLECTION)
    .find({ "createdById": req.params.id })
    .sort({ "createdAt": -1 })
    .toArray(function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to retrieve user projects");
      } else {
        res.status(200).json(doc);
      }
    });
});











