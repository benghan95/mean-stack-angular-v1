var app = angular.module('city360App', ['ui.router', 'multipleSelect', 'google.places', 'ngSanitize', 'ngCookies', 'textAngular', '720kb.socialshare', 'ui.bootstrap']);
  app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state({
        name: 'home',
        url: '/',
        views: {
          'header': {
            templateUrl: 'views/header-homepage.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/homepage.html',
            controller: 'HomeCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'projects',
        url: '/projects',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/projects-list.html',
            controller: 'ListProjectsCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'project',
        url: '/project/:projectId',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/project.html',
            controller: 'ProjectCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'categories',
        url: '/categories',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/categories-list.html',
            controller: 'ListCategoriesCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'categoryProjectsList',
        url: '/categories/:category/projects',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/category-projects.html',
            controller: 'ListCategoryProjectsCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'newProject',
        url: '/projects/new',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/create-new-project.html',
            controller: 'NewProjectCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'editProject',
        url: '/projects/edit/:projectId',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/edit-project.html',
            controller: 'EditProjectCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'userProfile',
        url: '/user/profile/:userId',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderCtrl'
          },
          'content': {
            templateUrl: 'views/user-profile.html',
            controller: 'UserProfileCtrl'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state({
        name: 'adminLogin',
        url: '/admin/login',
        views: {
          'content': {
            templateUrl: 'views/admin-login.html',
            controller: 'AdminLoginCtrl'
          },
          'footer': {
            templateUrl: 'views/footer-admin.html'
          }
        }
      })
      .state({
        name: 'adminDashboard',
        url: '/admin/dashboard',
        views: {
          'header': {
            templateUrl: 'views/header-admin.html',
            controller: 'AdminHeaderCtrl'
          },
          'content': {
            templateUrl: 'views/admin-dashboard.html',
            controller: 'AdminDashboardCtrl'
          },
          'footer': {
            templateUrl: 'views/footer-admin.html'
          }
        }
      })
      .state({
        name: 'adminProjects',
        url: '/admin/projects',
        views: {
          'header': {
            templateUrl: 'views/header-admin.html',
            controller: 'AdminHeaderCtrl'
          },
          'content': {
            templateUrl: 'views/admin-dashboard.html',
            controller: 'AdminDashboardCtrl'
          },
          'footer': {
            templateUrl: 'views/footer-admin.html'
          }
        }
      })
      .state({
        name: 'adminAnalytics',
        url: '/admin/analytics',
        views: {
          'header': {
            templateUrl: 'views/header-admin.html'
          },
          'content': {
            templateUrl: 'views/admin-dashboard.html',
            controller: 'AdminDashboardCtrl'
          },
          'footer': {
            templateUrl: 'views/footer-admin.html'
          }
        }
      });
  });
  app.config(['$httpProvider', function($httpProvider) {
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
  ]);
  //Header Controller
  app.controller("HeaderCtrl", function($scope, $state, $http, $cookies) {

    console.log("You are using HeaderCtrl.");

    $scope.user = {
      loggedIn: false,
      name: null
    };

    var retrieveCookies = $cookies.get('city360username');

    if(retrieveCookies == null){
      $scope.user.loggedIn = false;
    } else{
      $scope.user.loggedIn = true;
      console.log("User has logged in before.");
      $scope.user.name = $cookies.get('city360name');
    }

    $scope.userLogin = function(){
      console.log("Login button is clicked.");

      $http.get("http://city360.herokuapp.com/user/auth/" + $scope.user.email)
        .then(function(res){
          
          if (res.data.password == $scope.user.password) {
            console.log("Password entered is correct!");

            //You can set the expired time with the third params
            var today = new Date();
            var expired = new Date(today);
            expired.setDate(today.getDate() + 7); //Set expired date to 1 week
            $cookies.put('city360name', res.data.name, { expires : expired });
            $cookies.put('city360username', res.data.username, { expires : expired });
            $cookies.put('city360id', res.data._id, { expires : expired });
            console.log("User's details is stored in cookies.");
            console.log("User is now logged in.");
            $scope.loggedIn = true;
            $('.modal-backdrop').remove();
            $('.modal-open').removeClass("modal-open");
            $state.go('home', {}, { reload: true });
            $state.reload();
          }
          
        }, function(res){
          console.log("Error retrieving users information.");
        });
    }
    $scope.userLogout = function(){
      console.log("Logout button is clicked.");
      $cookies.remove('city360name', {});
      $cookies.remove('city360username', {});
      $cookies.remove('city360id', {});
      $state.reload();
      $state.go('home', {}, { reload: true });
    }
  })
  // Home Controller
  .controller("HomeCtrl", function($scope, $state, $http, $stateParams) {
    
    console.log("You are using HomeCtrl.");

    $scope.user = {};

    $scope.projects = [];

    var getLatestProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/latest/project")
        .then(function(res){
          console.log(res.data);
          if(res.data[0] != null){
            $scope.latestProject1 = res.data[0];
          } else{
            $scope.latestProject1 = null;
          }
          if(res.data[1] != null){
            $scope.latestProject2 = res.data[1];
          } else{
            $scope.latestProject2 = null;
          }
          if(res.data[2] != null){
            $scope.latestProject3 = res.data[2];
          } else{
            $scope.latestProject3 = null;
          }
        }, function(res){
          console.log("Error retrieving latest projects.");
        });
      console.log("The latest projects' data is loaded.");
    };

    console.log("Retrieving the latest projects data.");
    getLatestProjects();
    console.log("The latest projects' data is retrieved.");

    var getPopularProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/popular/project")
        .then(function(res){
          console.log(res.data);
          if(res.data[0] != null){
            $scope.popularProject1 = res.data[0];
          } else{
            $scope.popularProject1 = null;
          }
          if(res.data[1] != null){
            $scope.popularProject2 = res.data[1];
          } else{
            $scope.popularProject2 = null;
          }
          if(res.data[2] != null){
            $scope.popularProject3 = res.data[2];
          } else{
            $scope.popularProject3 = null;
          }
        }, function(res){
          console.log("Error retrieving popular projects.");
        });
      console.log("Popular projects' data is loaded.");
    };

    console.log("Retrieving popular projects data.");
    getPopularProjects();
    console.log("Popular projects' data is retrieved.");


    $scope.signUp = function(){
      console.log("Sign up button is clicked!");

      var postObject = new Object();

      postObject.name = $scope.user.name;
      postObject.username = $scope.user.username;
      postObject.email = $scope.user.email;
      postObject.password = $scope.user.password;

      console.log(postObject);

      var req = {
        method: 'POST',
        url: 'http://city360.herokuapp.com/users',
        data: postObject
      };

      $http(req).success(function(res) {
        console.log('Success', res);
        $('.modal-backdrop').remove();
        $state.go('home', {}, { reload: true });
      }).error(function(err) {
        console.error('ERROR', err);
      })
    };

  })
  // List Projects Controller
  .controller("ListProjectsCtrl", function($scope, $state, $http, $cookies) {
    
    console.log("You are using ListProjectsCtrl.");

    $scope.projects = [];
    $scope.user = {};

    $scope.user = {
      loggedIn: false,
      username: null
    };

    var retrieveCookies = $cookies.get('city360username');

    if(retrieveCookies == null){
      $scope.user.loggedIn = false;
    } else{
      $scope.user.loggedIn = true;
      $scope.user.username = $cookies.get('city360username');
    }

    $scope.editProfile = function(){
      console.log("Edit profile button is clicked.");
      $state.go('userProfile', { "userId": retrieveCookies }, { reload: true });
    }

    var getProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/")
        .then(function(res){
          $scope.projects = res.data;
          console.log(res.data);
        }, function(res){
          console.log("Error retrieving projects.");
        });
      console.log("Project data is loaded.");
    };

    console.log("Getting projects data.");
    getProjects();
    console.log("Projects data is retrieved.");

    $scope.createNewProject = function(){
      console.log("Create new project button is clicked.");
      $state.go('newProject', {}, { reload: true });
    }

    var hasLiked = false;

    $scope.likeClicked = function(){
      if (!hasLiked) {
        hasLiked = true;
        $scope.likedCount += 1;
        console.log("You liked the project! :)");
      } else {
        hasLiked = false;
        $scope.likedCount -= 1;
        console.log("You unliked the project. ):");
      }
    }
  })
  // Single Project Controller
  .controller("ProjectCtrl", function($scope, $state, $http, $stateParams){

    console.log("You are using ProjectCtrl.");
    console.log($stateParams);
    console.log($stateParams.projectId);

    $scope.comments = null;
    $scope.data = {};
    $scope.user = {};
    $scope.user.ownProject = true;

    console.log("Retrieving project details...");
    //retrieve project details
    $http.get("http://city360.herokuapp.com/projects/" + $stateParams.projectId)
      .then(function(res) {
        $scope.project = res.data;
        console.log("Retrieved project details will be shown.");
        console.log(res.data);
      }, function(res) {
        console.log("Error retrieving data.");
      });

    // retrieve project comments
    var retrieveComments = function(){
      $http.get("http://city360.herokuapp.com/comments/" + $stateParams.projectId)
      .then(function(res) {
        if(res.data.length != 0){
          $scope.comments = res.data;
        }
        console.log(res.data);
        
      }, function(res) {
        console.log("Error retrieving data.");
      });
    }
    console.log("Retrieving project comments...");
    retrieveComments();

    // submit comment function
    $scope.submitComment = function(){
      console.log("Submit comment button clicked.");

      var postObject = new Object();

      postObject.userId = window.localStorage['username'];
      postObject.projectId = $stateParams.projectId;
      postObject.comment = $scope.comment.text;

      console.log(postObject);

      var req = {
        method: 'POST',
        url: 'http://city360.herokuapp.com/comments',
        data: postObject
      };

      $http(req).success(function(res){
        console.log('Success', res);
        console.log("Comment is submitted.");
        retrieveComments();
        console.log("Posted new comment.");
      }).error(function(err){
        console.error('Error', err);
      });
    };
  })
  // List Category Projects Controller
  .controller("ListCategoryProjectsCtrl", function($scope, $state, $http, $stateParams, $cookies) {
    
    console.log("You are using ListCategoryProjectsCtrl.");

    console.log($stateParams);
    $scope.category = null;
    $scope.category = $stateParams.category;
    $scope.projects = [];
    $scope.user = {};

    var retrieveCookies = $cookies.get('city360id');

    var getUserInfo = function(){
      $http.get("http://city360.herokuapp.com/user/" + retrieveCookies)
        .then(function(res){
          $scope.user = res.data;
          console.log(res.data);
        }, function(res){
          console.log("Error retrieving user's information.");
        });
      console.log("User's information is loaded.");
    };

    if(retrieveCookies == null){
      $scope.user.loggedIn = false;
    } else{
      $scope.user.loggedIn = true;
      getUserInfo();
    }

    $scope.editProfile = function(){
      console.log("Edit profile button is clicked.");
      $state.go('userProfile', { "userId": retrieveCookies }, { reload: true });
    }

    var getProjects = function(){
      $http.get("http://city360.herokuapp.com/categories/" + $stateParams.category + "/projects")
        .then(function(res){
          $scope.projects = res.data;
          console.log(res.data);
        }, function(res){
          console.log("Error retrieving projects of category " + $stateParams.category + ".");
        });
      console.log("All the projects with " + $stateParams.category + " category is loaded.");
    };

    console.log("Getting projects data of category " + $stateParams.category + ".");
    getProjects();
    console.log("Projects data with " + $stateParams.category + " category is retrieved.");

    $scope.createNewProject = function(){
      console.log("Create new project button is clicked.");
      $state.go('newProject', {}, { reload: true });
    }

    var hasLiked = false;

    $scope.likeClicked = function(){
      if (!hasLiked) {
        hasLiked = true;
        $scope.likedCount += 1;
        console.log("You liked the project! :)");
      } else {
        hasLiked = false;
        $scope.likedCount -= 1;
        console.log("You unliked the project. ):");
      }
    }
  })
  // New Project Controller
  .controller("NewProjectCtrl", function($scope, $state, $http, $cookies, textAngularManager) {
    
    console.log("You are using NewProjectCtrl.");

    $scope.version = textAngularManager.getVersion();
    $scope.versionNumber = $scope.version.substring(1);
    $scope.project = {};
    $scope.project.founder = {};
    $scope.user = {
      loggedIn: false,
      createdBy: null
    };
    $scope.project.description = 'Hello World!';

    $scope.categoryList = [
      "Technology",
      "Environment",
      "Education",
      "Community",
      "Infrastructure & Facilities",
      "Fitness & Health",
      "Safety"
    ];

    $scope.submit = function(){

      console.log("Submit new project is clicked.");

      var postObject = new Object();

      var retrieveCookies = $cookies.get('city360id');

      if(retrieveCookies == null){
        $scope.user.loggedIn = false;
      } else{
        $scope.user.loggedIn = true;
        postObject.createdBy = retrieveCookies;
      }

      postObject.title = $scope.project.title;
      postObject.description = $scope.project.description;
      postObject.excerpt = $scope.project.excerpt;
      postObject.categories = $scope.project.categories;
      postObject.location = $scope.project.location;
      postObject.startingDateTime = $scope.project.startingDateTime;
      postObject.endingDateTime = $scope.project.endingDateTime;
      postObject.imageURL = $scope.project.imageURL;
      postObject.noOfPax = $scope.project.manpower;
      postObject.foundedBy = $cookies.get('city360name');
      postObject.contactEmail = $scope.project.founder.email;
      postObject.contactNo = $scope.project.founder.phoneNo;
      postObject.status = $scope.project.status;
      postObject.createdAt = new Date();
      postObject.approval = "Waiting for Approval";
      postObject.liked = [];
      postObject.likesCount = 0;

      console.log(postObject);

      var req = {
        method: 'POST',
        url: 'http://city360.herokuapp.com/projects',
        data: postObject
      };

      $http(req).success(function(res){
        console.log('Success', res);
        console.log("Project is created and submitted.");
        console.log(res._id);
        $state.go('project', { 'projectId': res._id }, { reload: true });
      }).error(function(err){
        console.error('Error', err);
      });
    };
  })
  .controller("EditProjectCtrl", function($scope, $state, $http, $stateParams, textAngularManager) {

    console.log("You are using EditProjectCtrl.");

    $scope.version = textAngularManager.getVersion();
    $scope.versionNumber = $scope.version.substring(1);
    $scope.project = {};
    $scope.project.founder = {};
    $scope.project.description = 'Hello World!';

    $scope.categoryList = [
      "Technology",
      "Environment",
      "Education",
      "Community",
      "Infrastructure & Facilities",
      "Fitness & Health",
      "Safety"
    ];

    console.log("Retrieving project details...");
    //retrieve project details
    $http.get("http://city360.herokuapp.com/projects/" + $stateParams.projectId)
      .then(function(res) {
        $scope.project = res.data;
        console.log("Retrieved project details will be shown.");
        console.log(res.data);
      }, function(res) {
        console.log("Error retrieving data.");
      });

    $scope.submit = function(){

      console.log("Submit new project is clicked.");

      var postObject = new Object();

      var retrieveCookies = $cookies.get('city360id');

      if(retrieveCookies == null){
        $scope.user.loggedIn = false;
      } else{
        $scope.user.loggedIn = true;
        postObject.createdBy = retrieveCookies;
      }

      postObject.title = $scope.project.title;
      postObject.description = $scope.project.description;
      postObject.excerpt = $scope.project.description.substring(0, 400) + "...";
      postObject.categories = $scope.project.categories;
      postObject.location = $scope.project.location;
      postObject.startingDateTime = $scope.project.startingDateTime;
      postObject.endingDateTime = $scope.project.endingDateTime;
      postObject.imageURL = $scope.project.imageURL;
      postObject.noOfPax = $scope.project.manpower;
      postObject.foundedBy = $cookies.get('city360name');
      postObject.contactEmail = $scope.project.founder.email;
      postObject.contactNo = $scope.project.founder.phoneNo;
      postObject.status = $scope.project.status;
      postObject.createdAt = new Date();
      postObject.liked = [];
      postObject.likesCount = 0;

      console.log(postObject);

      var req = {
        method: 'POST',
        url: 'http://city360.herokuapp.com/projects',
        data: postObject
      };

      $http(req).success(function(res){
        console.log('Success', res);
        console.log("Project is created and submitted.");
        console.log(res._id);
        $state.go('project', { 'projectId': res._id }, { reload: true });
      }).error(function(err){
        console.error('Error', err);
      });
    };
  })
  .controller("ListCategoriesCtrl", function($scope, $state, $http){
    
  })
  .controller("UserProfileCtrl", function($scope, $state, $http, $stateParams){

    console.log("You are using UserProfileCtrl.");

    $scope.userProfile = {};

    console.log("Retrieving user details...");
    //retrieve project details
    $http.get("http://city360.herokuapp.com/user/" + $stateParams.userId)
      .then(function(res) {
        $scope.userProfile = res.data;
        console.log("Retrieved user details will be shown.");
        console.log(res.data);
      }, function(res) {
        console.log("Error retrieving user details.");
      });
  })
  .controller("ListCategoryProjectsCtrl", function($scope, $state, $http, $cookies) {
    
    console.log("You are using ListCategoryProjectsCtrl.");

    $scope.projects = [];
    $scope.user = {};

    var retrieveCookies = $cookies.get('city360id');

    var getUserInfo = function(){
      $http.get("http://city360.herokuapp.com/user/" + retrieveCookies)
        .then(function(res){
          $scope.user = res.data;
          console.log(res.data);
        }, function(res){
          console.log("Error retrieving user's information.");
        });
      console.log("User's information is loaded.");
    };

    if(retrieveCookies == null){
      $scope.user.loggedIn = false;
    } else{
      $scope.user.loggedIn = true;
      $scope.user.username = $cookies.get('city360username');
      getUserInfo();
    }

    $scope.editProfile = function(){
      console.log("Edit profile button is clicked.");
      $state.go('userProfile', { "userId": retrieveCookies }, { reload: true });
    }

    var getProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/")
        .then(function(res){
          $scope.projects = res.data;
          console.log(res.data);
        }, function(res){
          console.log("Error retrieving projects.");
        });
      console.log("Project data is loaded.");
    };

    console.log("Getting projects data.");
    getProjects();
    console.log("Projects data is retrieved.");

    $scope.createNewProject = function(){
      console.log("Create new project button is clicked.");
      $state.go('newProject', {}, { reload: true });
    }

    var hasLiked = false;

    $scope.likeClicked = function(){
      if (!hasLiked) {
        hasLiked = true;
        $scope.likedCount += 1;
        console.log("You liked the project! :)");
      } else {
        hasLiked = false;
        $scope.likedCount -= 1;
        console.log("You unliked the project. ):");
      }
    }
  })
  .controller("AdminHeaderCtrl", function($scope, $state, $http, $cookies) {

    console.log("You are using AdminHeaderCtrl.");

    $scope.admin = {
      loggedIn: false,
      name: null
    };

    var retrieveCookies = $cookies.get('city360admin');

    if(retrieveCookies == null){
      $scope.admin.loggedIn = false;
    } else{
      $scope.admin.loggedIn = true;
      console.log("Admin has logged in before.");
      $scope.admin.name = $cookies.get('city360adminname');
    }

    $scope.adminLogout = function(){
      console.log("Logout button is clicked.");
      $cookies.remove('city360adminname', {});
      $cookies.remove('city360adminemail', {});
      $cookies.remove('city360admin', {});
      $state.reload();
      $state.go('home', {}, { reload: true });
    }
  })
  .controller("AdminLoginCtrl", function($scope, $state, $http, $stateParams, $cookies){
    console.log("You are using AdminLoginCtrl.");

    $scope.admin = {
      loggedIn: false,
      name: null,
      email: null
    };

    var retrieveAdmin = $cookies.get('city360admin');

    if(retrieveAdmin == null){
      
    } else{
      $scope.admin.loggedIn = true;
      $state.go('adminDashboard', {}, { reload: true });
    }

    $scope.adminLogin = function(){
      console.log("Login button is clicked.");

      $http.get("http://city360.herokuapp.com/admin/auth/" + $scope.admin.email)
        .then(function(res){
          
          if (res.data.password == $scope.admin.password) {
            console.log("Password entered is correct!");

            //You can set the expired time with the third params
            var today = new Date();
            var expired = new Date(today);
            expired.setDate(today.getDate() + 7); //Set expired date to 1 week
            $cookies.put('city360adminname', res.data.name, { expires : expired });
            $cookies.put('city360adminemail', res.data.username, { expires : expired });
            $cookies.put('city360admin', res.data._id, { expires : expired });
            console.log("Admin's details is stored in cookies.");
            console.log("Admin is now logged in.");
            $scope.loggedIn = true;
            $('.modal-backdrop').remove();
            $('.modal-open').removeClass("modal-open");
            $state.go('home', {}, { reload: true });
            $state.reload();
          }
          
        }, function(res){
          console.log("Error retrieving admin's information.");
        });
    }
  })
  // Single Project Controller
  .controller("AdminDashboardCtrl", function($scope, $state, $http, $stateParams, $cookies) {
    
    console.log("You are using AdminDashboardCtrl.");

    $scope.projects = [];
    $scope.newProjects = [];
    $scope.approvedProjects = [];
    $scope.removedProjects = [];
    $scope.admin = {
      loggedIn: false,
      name: null
    };

    var retrieveAdmin = $cookies.get('city360admin');

    if(retrieveAdmin == null){
      $state.go('adminLogin', {}, { reload: true });
    } else{
      $scope.admin.name = $cookies.get('city360adminname');
      $scope.admin.loggedIn = true;
    }
    var getProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/")
        .then(function(res){
          $scope.projects = res.data
          console.log($scope.newProjects);
        }, function(res){
          console.log("Error retrieving projects.");
        });
      console.log("New Project data is loaded.");
    };

    var getApprovedProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/")
        .then(function(res){
          for (var i = 0; i < res.data.length; i++){
            if(res.data[i].approval == "Approved"){
              $scope.approvedProjects.push(res.data[i]);
            }
          }
          console.log($scope.newProjects);
        }, function(res){
          console.log("Error retrieving projects.");
        });
      console.log("New Project data is loaded.");
    };

    var getRemovedProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/")
        .then(function(res){
          for (var i = 0; i < res.data.length; i++){
            if(res.data[i].approval == "Declined"){
              $scope.newProjects.push(res.data[i]);
            }
          }
          console.log($scope.newProjects);
        }, function(res){
          console.log("Error retrieving projects.");
        });
      console.log("New Project data is loaded.");
    };

    var getNewProjects = function(){
      $http.get("http://city360.herokuapp.com/projects/")
        .then(function(res){
          for (var i = 0; i < res.data.length; i++){
            if(res.data[i].approval == "Waiting for Approval"){
              $scope.newProjects.push(res.data[i]);
            }
          }
          console.log($scope.newProjects);
        }, function(res){
          console.log("Error retrieving projects.");
        });
      console.log("New Project data is loaded.");
    };

    console.log("Getting new projects data.");
    getNewProjects();
    console.log("Projects data is retrieved.");
  });












